package client.keycloak.externalapi;

import client.keycloak.payload.request.UserRequest;
import client.keycloak.payload.response.KeyCloakUserDetailResponse;
import client.keycloak.payload.response.TokenResponse;
import java.io.IOException;

import com.fasterxml.jackson.core.type.TypeReference;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;
import client.keycloak.util.JacksonUtil;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static client.keycloak.util.TestResponse.TOKEN_RESPONSE;
import static client.keycloak.util.TestResponse.USER_DETAIL_RESPONSE;
import static client.keycloak.util.TestResponse.USER_REQUEST;

class KeycloakApiTest {

  private MockWebServer mockWebServer;
  private KeycloakApi keycloakApi;
  private String url = "";
  private JacksonUtil jacksonUtil;

  @BeforeEach
  void setUp() throws IOException {
    mockWebServer = new MockWebServer();
    mockWebServer.start();
    keycloakApi = new KeycloakApi(WebClient.builder().build());
    url = String.format("http://localhost:%s", mockWebServer.getPort());
    jacksonUtil = new JacksonUtil();
  }

  @AfterEach
  void tearDown() throws IOException {
    mockWebServer.shutdown();
  }

  @Test
  void getClientTokenTest() {
    mockWebServer.enqueue(
        new MockResponse().setResponseCode(200)
            .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .setBody(TOKEN_RESPONSE)
    );

    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("grant_type", "");
    bodyValues.add("client_id", "");
    bodyValues.add("client_secret", "");

    Mono<TokenResponse> tokenResponse = keycloakApi.getClientToken(bodyValues, url);

    StepVerifier
        .create(tokenResponse)
        .assertNext(token -> {
          assertEquals("access_token", token.accessToken());
          assertEquals("refresh_token", token.refreshToken());
        })
        .verifyComplete();
  }

  @Test
  void createUserTest() {
    mockWebServer.enqueue(
        new MockResponse().setResponseCode(201)
            .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
    );

    Mono<ResponseEntity<Void>> user = jacksonUtil
    .deserializeObject(USER_REQUEST, new TypeReference<UserRequest>() {})
    .flatMap(u -> keycloakApi.createUser(u, "token", url));

    StepVerifier
        .create(user)
        .expectSubscription()
        .assertNext(status -> assertEquals(201, status.getStatusCode().value()))
        .verifyComplete();
  }

  @Test
  void getUserIdTest() {
    mockWebServer.enqueue(
        new MockResponse().setResponseCode(200)
            .setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .setBody(USER_DETAIL_RESPONSE)
    );

    Mono<KeyCloakUserDetailResponse> users = keycloakApi.getUserDetail("0720625955", "token", url);

    StepVerifier
        .create(users)
        .expectSubscription()
        .expectNextCount(1)
        .verifyComplete();
  }
}