package client.keycloak.util;

public class TestResponse {
  public static String TOKEN_RESPONSE = """
      {
      	"access_token": "access_token",
      	"expires_in": 900,
      	"refresh_expires_in": 1800,
      	"refresh_token": "refresh_token",
      	"token_type": "bearer",
      	"not-before-policy": 0,
      	"session_state": "32d07c84-5ff3-4bc7-b8d0-328977dc5cac",
      	"scope": ""
      }
      """;

  public static String USER_REQUEST= """
      {
      	"username": "0720625955",
      	"enabled": true,
      	"emailVerified": true,
      	"credentials": [
             {
                 "type": "password",
                 "value": "perminus12",
                 "temporary": false
             }
         ]
      }
      """;

  public static String USER_DETAIL_RESPONSE= """
      [
       	{
       		"id": "397b5c40-bf60-447f-9b51-e59b2ebc9dee",
       		"createdTimestamp": 1673684518596,
       		"username": "0720625955",
       		"enabled": true,
       		"totp": false,
       		"emailVerified": true,
       		"disableableCredentialTypes": [],
       		"requiredActions": [],
       		"notBefore": 0,
       		"access": {
       			"manageGroupMembership": true,
       			"view": true,
       			"mapRoles": true,
       			"impersonate": true,
       			"manage": true
       		}
       	}
       ]
      """;
}
