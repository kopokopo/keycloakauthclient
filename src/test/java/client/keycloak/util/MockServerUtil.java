package client.keycloak.util;

import java.io.IOException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

public class MockServerUtil {
  private MockWebServer mockWebServer;

  public MockWebServer mockWebServer() {
    return new MockWebServer();
  }

  public void startServer() throws IOException {
    if (mockWebServer == null) {
      mockWebServer();
    }

    mockWebServer.start();
  }

  public void stopServer() throws IOException {
    if (mockWebServer != null) {
      mockWebServer.shutdown();
    }
  }

  public MockResponse mockResponse(HttpStatus httpStatus, HttpHeaders httpHeaders) {
    return null;
  }
}
