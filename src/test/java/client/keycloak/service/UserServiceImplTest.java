package client.keycloak.service;

import client.keycloak.model.User;
import client.keycloak.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class UserServiceImplTest {

  @Mock
  UserRepository repository;

  @InjectMocks
  UserServiceImpl userService;

  User u;

  @BeforeEach
  void setUp() {
    userService = new UserServiceImpl(repository);
    u = new User();
    u.setAuthId("perminus12");
    u.setId("perminus");
  }

  @AfterEach
  void tearDown() {
    u = null;
  }

  @Test
  void save() {
    Mockito.when(repository.save(any(User.class))).thenReturn(Mono.just(u));

    Mono<User> user = repository.save(u);

    StepVerifier
        .create(user)
        .assertNext(newUser -> {
          assertEquals("perminus12", newUser.getAuthId());
          assertEquals("perminus", newUser.getId());
        })
        .verifyComplete();
  }

  @Test
  void getById() {
    Mockito.when(userService.getByAuthId(Mockito.anyString())).thenReturn(Mono.just(u));
    Mono<User> user = userService.getByAuthId("perminus");

    StepVerifier
        .create(user)
        .assertNext(newUser -> {
          assertEquals("perminus12", newUser.getAuthId());
          assertEquals("perminus", newUser.getId());
        })
        .verifyComplete();
  }
}