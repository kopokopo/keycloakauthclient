package client.keycloak.externalapi;

import java.net.URI;

import client.keycloak.exception.BadRequestException;
import client.keycloak.exception.IntegrationException;
import client.keycloak.exception.UnauthorizedException;
import client.keycloak.exception.UserRegistrationException;
import client.keycloak.payload.request.UserCredentialRequest;
import client.keycloak.payload.request.UserRequest;
import client.keycloak.payload.response.ClientRoleResponse;
import client.keycloak.payload.response.KeyCloakUserDetailResponse;
import client.keycloak.payload.response.TokenResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class KeycloakApi {
  private final WebClient webClient;

  public KeycloakApi(WebClient webClient) {
    this.webClient = webClient;
  }

  public Mono<TokenResponse> getClientToken(
      MultiValueMap<String, String> bodyValues,
      String url
  ) {
    try {
      return webClient.post()
          .uri(new URI(url.strip()))
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .accept(MediaType.APPLICATION_JSON)
          .body(BodyInserters.fromFormData(bodyValues))
          .retrieve()
          .onStatus(status ->  status.value() == 401, response -> response.bodyToMono(String.class).map(UnauthorizedException::new))
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
          .bodyToMono(TokenResponse.class);
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }

  public Mono<TokenResponse> getUserToken(
      MultiValueMap<String, String> bodyValues,
      String clientId,
      String secret,
      String url
  ) {
    try {
      return webClient.post()
          .uri(new URI(url.strip()))
          .headers(httpHeaders -> httpHeaders.setBasicAuth(Base64Utils
              .encodeToString((clientId + ":" + secret).getBytes(UTF_8))))
          .contentType(MediaType.APPLICATION_FORM_URLENCODED)
          .accept(MediaType.APPLICATION_JSON)
          .body(BodyInserters.fromFormData(bodyValues))
          .retrieve()
          .onStatus(status ->  status.value() == 401, response -> response.bodyToMono(String.class).map(UnauthorizedException::new))
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
          .bodyToMono(TokenResponse.class);
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }

  public Mono<ResponseEntity<Void>> createUser(
      UserRequest request,
      String token,
      String url
  ) {
    try {
      return webClient
          .post()
          .uri(new URI(url.strip().concat("/users")))
          .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token))
          .contentType(MediaType.APPLICATION_JSON)
          .accept(MediaType.APPLICATION_JSON)
          .body(Mono.just(request), UserRequest.class)
          .retrieve()
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(UserRegistrationException::new))
          .toBodilessEntity();
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }

  public Mono<KeyCloakUserDetailResponse> getUserDetail(
      String username,
      String token,
      String url
  ) {
    try {
      return webClient
          .get()
          .uri(url.strip().concat("/users"), uri -> uri
              .queryParam("username", username)
              .build()
          )
          .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token))
          .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
          .accept(MediaType.APPLICATION_JSON)
          .retrieve()
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
          .bodyToFlux(KeyCloakUserDetailResponse.class).next();
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }

  public Flux<ClientRoleResponse> getClientRoles(
      String clientId,
      String token,
      String url
  ) {
    try {
      return webClient
          .get()
          .uri(url.strip(), uri -> uri.path("/clients/{clientId}/roles").build(clientId))
          .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token))
          .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
          .accept(MediaType.APPLICATION_JSON)
          .retrieve()
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
          .bodyToFlux(ClientRoleResponse.class);
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }

  public Mono<ResponseEntity<Void>> addUserRole(
      Flux<ClientRoleResponse> clientRoles,
      String userId,
      String clientId,
      String token,
      String url
  ) {
    try {
      return webClient
          .post()
          .uri(url.strip(), uri -> uri.path("/users/{userId}/role-mappings/clients/{clientId}").build(userId, clientId))
          .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token))
          .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
          .accept(MediaType.APPLICATION_JSON)
          .body(clientRoles, ClientRoleResponse.class)
          .retrieve()
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
          .toBodilessEntity();
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }

  public Mono<ResponseEntity<Void>> updateUserPassword(
      UserCredentialRequest credentials,
      String userId,
      String token,
      String url
  ) {
    try {
      return webClient
          .put()
          .uri(url.strip(), uri -> uri.path("/users/{userId}/reset-password").build(userId))
          .header(HttpHeaders.AUTHORIZATION, "Bearer ".concat(token))
          .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
          .accept(MediaType.APPLICATION_JSON)
          .body(credentials, UserCredentialRequest.class)
          .retrieve()
          .onStatus(HttpStatusCode::is4xxClientError, response -> response.bodyToMono(String.class).map(BadRequestException::new))
          .onStatus(HttpStatusCode::is5xxServerError, response -> response.bodyToMono(String.class).map(IntegrationException::new))
          .toBodilessEntity();
    } catch (Exception e) {
      throw new IntegrationException(e.getMessage(), e);
    }
  }
}
