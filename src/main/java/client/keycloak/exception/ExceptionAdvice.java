package client.keycloak.exception;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.bind.support.WebExchangeBindException;
import reactor.core.publisher.Mono;

@RestControllerAdvice
public class ExceptionAdvice {

  @ExceptionHandler(NotFoundException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(NotFoundException e) {
    return ResponseEntity
        .status(HttpStatus.NOT_FOUND)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(WebExchangeBindException.class)
  public ResponseEntity<List<String>> handleException(WebExchangeBindException e) {
    var errors = e.getBindingResult()
        .getAllErrors()
        .stream()
        .map(DefaultMessageSourceResolvable::getDefaultMessage)
        .collect(Collectors.toList());
    return ResponseEntity.badRequest().body(errors);
  }

  @ExceptionHandler(IntegrationException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(IntegrationException e) {
    return ResponseEntity
        .status(HttpStatus.INTERNAL_SERVER_ERROR)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(UnauthorizedException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(UnauthorizedException e) {
    return ResponseEntity
        .status(HttpStatus.UNAUTHORIZED)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(BadRequestException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(BadRequestException e) {
    return ResponseEntity
        .status(HttpStatus.BAD_REQUEST)
        .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }

  @ExceptionHandler(DataViolationException.class)
  public ResponseEntity<Mono<ExceptionResponse>> error(DataViolationException e) {
    return ResponseEntity
            .status(HttpStatus.NOT_ACCEPTABLE)
            .body(Mono.just(new ExceptionResponse(List.of(e.getMessage()))));
  }
}
