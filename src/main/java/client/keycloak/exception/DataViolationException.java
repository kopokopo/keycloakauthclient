package client.keycloak.exception;

public class DataViolationException extends RuntimeException {
    public DataViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
