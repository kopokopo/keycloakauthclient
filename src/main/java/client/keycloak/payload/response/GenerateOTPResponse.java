package client.keycloak.payload.response;

public record GenerateOTPResponse(boolean status) {
}
