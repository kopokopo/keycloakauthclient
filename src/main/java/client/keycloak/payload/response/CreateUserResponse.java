package client.keycloak.payload.response;

public record CreateUserResponse(String userId) {
}
