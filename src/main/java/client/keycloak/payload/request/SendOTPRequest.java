package client.keycloak.payload.request;

import jakarta.validation.constraints.NotBlank;

public record SendOTPRequest(@NotBlank String userId) {
}
