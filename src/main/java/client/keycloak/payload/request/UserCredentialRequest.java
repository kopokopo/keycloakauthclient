package client.keycloak.payload.request;

public record UserCredentialRequest(String type, String value, boolean temporary) {
}
