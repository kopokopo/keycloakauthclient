package client.keycloak.payload.request;

import jakarta.validation.constraints.NotBlank;

public record DriverRegistrationRequest(
    @NotBlank(message = "Name is required") String name,
    @NotBlank(message = "Date of birth is required") String dob,
    @NotBlank(message = "Phone number is required") String phoneNumber,
    @NotBlank(message = "Password is required") String password
) {
}
