package client.keycloak.repository;

import client.keycloak.model.User;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;


public interface UserRepository extends ReactiveCrudRepository<User, String> {
    Mono<User> getByAuthId(String authId);
}
