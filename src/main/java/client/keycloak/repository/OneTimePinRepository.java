package client.keycloak.repository;

import client.keycloak.model.OneTimePin;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

public interface OneTimePinRepository extends ReactiveCrudRepository<OneTimePin, String> {
  Mono<OneTimePin> getByUserIdEqualsAndActiveTrue(String userId);
}
