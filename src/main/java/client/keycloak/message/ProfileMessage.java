package client.keycloak.message;

public record ProfileMessage(String userId, String name, String dob, String phoneNumber, Integer age, String email) {}
