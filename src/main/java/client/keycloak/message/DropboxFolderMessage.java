package client.keycloak.message;

public record DropboxFolderMessage(String userId, String username) {
}
