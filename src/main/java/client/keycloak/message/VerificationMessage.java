package client.keycloak.message;

public record VerificationMessage(String userId, String verificationTypeId, boolean status) {
}
