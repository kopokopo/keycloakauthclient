package client.keycloak.publisher;

import client.keycloak.message.DropboxFolderMessage;
import client.keycloak.message.VerificationMessage;
import org.springframework.stereotype.Component;

import client.keycloak.util.QueueDeclaration;
import jakarta.annotation.PreDestroy;
import reactor.core.publisher.Mono;
import reactor.rabbitmq.OutboundMessage;
import reactor.rabbitmq.Sender;
import client.keycloak.message.ProfileMessage;
import client.keycloak.util.JacksonUtil;

@Component
public class RabbitMQPublisher {
    private final Sender sender;
    private final JacksonUtil jacksonUtil;

    public RabbitMQPublisher(Sender sender, JacksonUtil jacksonUtil) {
        this.sender = sender;
        this.jacksonUtil = jacksonUtil;
    }

    public Mono<Void> publishMessage(String exchange, String routingKey, String message) {
        return sender
                .sendWithPublishConfirms(Mono.just(new OutboundMessage(exchange, routingKey, message.getBytes())))
                .doOnError(error -> System.out.println(error.getMessage()))
                //    .flatMap(m -> m.isAck())
                .then();
    }

    public void sendProfileMessage(String userId, String name, String dob, String phoneNumber, Integer age, String email) {
        Mono.fromCallable(() -> jacksonUtil.serializeObject(new ProfileMessage(userId, name, dob, phoneNumber, age, email)))
                .flatMap(m -> publishMessage(QueueDeclaration.DM_EXCHANGE, "PROFILE_QUEUE_ROUTING_KEY", m))
                .subscribe();
    }

    public void sendDropboxFolderMessage(String userId, String phoneNumber) {
        Mono.fromCallable(() -> jacksonUtil.serializeObject(new DropboxFolderMessage(userId, phoneNumber)))
                .flatMap(m -> publishMessage(QueueDeclaration.DM_EXCHANGE, "DROPBOX_FOLDER_QUEUE_ROUTING_KEY", m))
                .subscribe();
    }

    public void sendVerificationMessage(String userId, boolean status, String verificationTypeId) {
        Mono.fromCallable(() -> jacksonUtil.serializeObject(new VerificationMessage(userId, verificationTypeId, status)))
                .flatMap(m -> publishMessage(QueueDeclaration.DM_EXCHANGE, "VERIFICATION_QUEUE_ROUTING_KEY", m))
                .subscribe();
    }

    @PreDestroy
    public void close() {
        sender.close();
    }
}