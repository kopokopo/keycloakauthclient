package client.keycloak.util;

import java.util.HashMap;

public class QueueDeclaration {
    public static final String DM_EXCHANGE = "DM_EXCHANGE";
    public static final String DM_DEADLETTER_EXCHANGE = "DM_DEADLETTER_EXCHANGE";
    public static final String DM_DEADLETTER_QUEUE = "DM_DEADLETTER_QUEUE";


    public static HashMap<String, String> queueMap() {
        HashMap<String, String> q = new HashMap<>();

        q.put("PROFILE_QUEUE_ROUTING_KEY", "PROFILE_QUEUE");
        q.put("DROPBOX_FOLDER_QUEUE_ROUTING_KEY", "DROPBOX_FOLDER_QUEUE");
        q.put("VERIFICATION_QUEUE_ROUTING_KEY", "VERIFICATION_QUEUE");
        return q;
    }
}
