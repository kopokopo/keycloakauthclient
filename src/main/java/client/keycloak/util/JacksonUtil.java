package client.keycloak.util;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Mono;


@Component
public class JacksonUtil {
  private ObjectMapper mapper = new ObjectMapper();

  public <T> T deserializeObject(String json, TypeReference<T> typeReference) {
    try {
      return mapper.readValue(json, typeReference);
    } catch (Exception e) {
      throw new RuntimeException("Could not deserialize object!", e);
    }
  }

  public String serializeObject(Object targetClass) {
    try {
      return mapper.writeValueAsString(targetClass);
    } catch (Exception e) {
      throw new RuntimeException("Could not serialize object!", e);
    }
  }
}
