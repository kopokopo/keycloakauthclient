package client.keycloak.util;

import client.keycloak.externalapi.KeycloakApi;
import client.keycloak.payload.request.UserRequest;
import client.keycloak.payload.response.KeyCloakUserDetailResponse;
import client.keycloak.payload.response.TokenResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import reactor.core.publisher.Mono;

@Component
public class KeycloakUtil {
  @Value("${keycloak.grant_type}")
  private String grantType;

  @Value("${keycloak.client_id}")
  private String clientId;

  @Value("${keycloak.client_secret}")
  private String clientSecret;

  @Value("${keycloak.user.grant_type}")
  private String userGrantType;

  @Value("${keycloak.token.url}")
  private String keycloakUrl;

  @Value("${keycloak.base.url}")
  private String keycloakBaseUrl;

  @Value("${keycloak.internal.client.id}")
  private String keycloakClientId;

  private final KeycloakApi keycloakApi;

  public KeycloakUtil(KeycloakApi keycloakApi) {
    this.keycloakApi = keycloakApi;
  }

  public Mono<TokenResponse> getClientToken() {
    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("grant_type", grantType);
    bodyValues.add("client_id", clientId);
    bodyValues.add("client_secret", clientSecret);
    return keycloakApi.getClientToken(bodyValues, keycloakUrl);
  }

  public Mono<TokenResponse> getUserToken(String username, String password) {
    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
    bodyValues.add("grant_type", userGrantType);
    bodyValues.add("username", username);
    bodyValues.add("password", password);
    return keycloakApi.getUserToken(bodyValues, clientId, clientSecret, keycloakUrl);
  }

  public Mono<KeyCloakUserDetailResponse> getUserDetail(String username, String token) {
    return keycloakApi.getUserDetail(username, token, keycloakBaseUrl);
  }

  /**
   * returns keycloak userId
   *
   * @param request
   * @param token
   * @return
   */
  public Mono<KeyCloakUserDetailResponse> addUser(UserRequest request, String token) {
    return keycloakApi.createUser(request, token, keycloakBaseUrl)
        .flatMap(r -> keycloakApi.getUserDetail(request.username(), token, keycloakBaseUrl));
  }
}


