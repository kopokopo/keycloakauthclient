package client.keycloak.util;

import client.keycloak.model.User;
import client.keycloak.payload.request.UserRequest;
import client.keycloak.service.UserServiceImpl;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AccountUtil {
    private final KeycloakUtil keycloakUtil;
    private final UserServiceImpl userService;

    public AccountUtil(KeycloakUtil keycloakUtil, UserServiceImpl userService) {
        this.keycloakUtil = keycloakUtil;
        this.userService = userService;
    }

    public Mono<User> createUser(UserRequest request) {
        return keycloakUtil
                .getClientToken()
                .zipWhen(t -> keycloakUtil.addUser(request, t.accessToken()))
                .flatMap(u -> userService.save(u.getT2().id()));
    }
}
