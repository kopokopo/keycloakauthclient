package client.keycloak.util;

import java.security.SecureRandom;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

import reactor.core.publisher.Mono;

public class Helpers {

    public static Mono<String> generateOTP() {
        return Mono.just(generateRandomPassword());
    }

    public static String generateRandomPassword() {
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        SecureRandom rnd = new SecureRandom();
        int targetStringLength = 5;

        StringBuilder sb = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    public static Integer getAge(String dob) {
        return Period.between(
                LocalDate.parse(dob, DateTimeFormatter.ofPattern("yyyy-dd-MM")),
                LocalDate.now()
        ).getYears();
    }
}
