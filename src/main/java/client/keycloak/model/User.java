package client.keycloak.model;

import client.keycloak.model.audit.DateAudit;
import jakarta.validation.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table(name = "Users")
public class User extends DateAudit {

  @Id
  @Column(value = "id")
  @NotBlank
  private String id;

  @Column(value = "authId")
  @NotBlank
  private String authId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getAuthId() {
    return authId;
  }

  public void setAuthId(String authId) {
    this.authId = authId;
  }
}
