package client.keycloak.model;

import client.keycloak.model.audit.DateAudit;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table(name = "rabbitmqmessagelog")
public class RabbitMQMessageLog extends DateAudit {
  @Id
  private String id;

  @Column("message")
  private String message;

  @Column("sent")
  private boolean sent = Boolean.FALSE;

  @Column("consumed")
  private boolean consumed = Boolean.FALSE;

  @Column("exchange")
  private String exchange;

  @Column("routingKey")
  private String routingKey;

  @Column("queue")
  private String queue;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public boolean isSent() {
    return sent;
  }

  public void setSent(boolean sent) {
    this.sent = sent;
  }

  public String getExchange() {
    return exchange;
  }

  public void setExchange(String exchange) {
    this.exchange = exchange;
  }

  public String getRoutingKey() {
    return routingKey;
  }

  public void setRoutingKey(String routingKey) {
    this.routingKey = routingKey;
  }

  public boolean isConsumed() {
    return consumed;
  }

  public void setConsumed(boolean consumed) {
    this.consumed = consumed;
  }

  public String getQueue() {
    return queue;
  }

  public void setQueue(String queue) {
    this.queue = queue;
  }
}
