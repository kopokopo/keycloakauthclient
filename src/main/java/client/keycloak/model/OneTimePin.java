package client.keycloak.model;

import client.keycloak.model.audit.DateAudit;
import jakarta.validation.constraints.NotBlank;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table(name = "onetimepin")
public class OneTimePin extends DateAudit {

  @Id
  @NotBlank
  @Column(value = "id")
  private String id;

  @NotBlank
  @Column(value = "token")
  private String token;

  @NotBlank
  @Column(value = "userid")
  private String userId;

  private boolean active = Boolean.FALSE;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }
}
