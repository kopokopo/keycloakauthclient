package client.keycloak.config;

import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.stereotype.Component;

import client.keycloak.util.QueueDeclaration;
import jakarta.annotation.PostConstruct;

@Component
public class RabbitMQQueueConfig {

    private final AmqpAdmin amqpAdmin;

    public RabbitMQQueueConfig(AmqpAdmin amqpAdmin) {
        this.amqpAdmin = amqpAdmin;
    }

    @PostConstruct
    public void setupQueues() {
        dlq();

        QueueDeclaration.queueMap()
                .forEach((key, destination) -> {
                    Exchange ex = ExchangeBuilder
                            .topicExchange(QueueDeclaration.DM_EXCHANGE)
                            .durable(Boolean.TRUE)
                            .build();
                    amqpAdmin.declareExchange(ex);

                    Queue q = QueueBuilder.durable(destination)
                            .deadLetterExchange(QueueDeclaration.DM_DEADLETTER_EXCHANGE)
                            .build();
                    amqpAdmin.declareQueue(q);

                    Binding b = BindingBuilder.bind(q)
                            .to(ex)
                            .with(key)
                            .noargs();
                    amqpAdmin.declareBinding(b);
                });
    }

    private void dlq() {

        FanoutExchange ex = ExchangeBuilder
                .fanoutExchange(QueueDeclaration.DM_DEADLETTER_EXCHANGE)
                .durable(Boolean.TRUE)
                .build();

        amqpAdmin.declareExchange(ex);

        Queue q = QueueBuilder
                .durable(QueueDeclaration.DM_DEADLETTER_QUEUE)
                .build();
        amqpAdmin.declareQueue(q);

        Binding b = BindingBuilder.bind(q).to(ex);
        amqpAdmin.declareBinding(b);
    }

}
