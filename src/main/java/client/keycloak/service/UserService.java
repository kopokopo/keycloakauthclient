package client.keycloak.service;

import client.keycloak.model.User;
import reactor.core.publisher.Mono;

public interface UserService {
  Mono<User> save(String authId);
  Mono<User> getByAuthId(String authId);
}
