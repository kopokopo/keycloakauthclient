package client.keycloak.service;

import java.time.Instant;

import client.keycloak.exception.DataViolationException;
import client.keycloak.exception.NotFoundException;
import client.keycloak.model.User;
import client.keycloak.repository.UserRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<User> save(String authId) {
        return Mono.just(new User()).map(u -> {
                    u.setAuthId(authId);
                    u.setCreatedAt(Instant.now());
                    u.setUpdatedAt(Instant.now());
                    return u;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<User> getByAuthId(String authId) {
        return repository
                .getByAuthId(authId)
                .switchIfEmpty(Mono.error(new NotFoundException("NFE-001")));
    }
}
