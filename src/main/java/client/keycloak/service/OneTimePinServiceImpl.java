package client.keycloak.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import client.keycloak.exception.BadRequestException;
import client.keycloak.exception.DataViolationException;
import client.keycloak.exception.NotFoundException;
import client.keycloak.model.OneTimePin;
import client.keycloak.repository.OneTimePinRepository;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class OneTimePinServiceImpl implements OneTimePinService {

    private final OneTimePinRepository repository;

    public OneTimePinServiceImpl(OneTimePinRepository repository) {
        this.repository = repository;
    }

    @Override
    public Mono<OneTimePin> save(String token, String userId) {
        return getOptional(userId).map(o -> {
                    o.setToken(token);
                    o.setUserId(userId);
                    o.setActive(Boolean.TRUE);
                    o.setUpdatedAt(Instant.now());
                    return o;
                })
                .flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    @Override
    public Mono<OneTimePin> getActiveByUserId(String userId) {
        return repository
                .getByUserIdEqualsAndActiveTrue(userId)
                .switchIfEmpty(Mono.error(new NotFoundException("NFE-001")));
    }

    @Override
    public Mono<OneTimePin> getOptional(String userId) {
        return repository
                .getByUserIdEqualsAndActiveTrue(userId)
                .switchIfEmpty(Mono.just(new OneTimePin()));
    }

    @Override
    public Mono<OneTimePin> updateOTPStatus(String userId, boolean status) {
        return getActiveByUserId(userId)
                .map(o -> {
                    o.setActive(status);
                    return o;
                }).flatMap(repository::save)
                .onErrorMap(ex -> new DataViolationException("DVE-001", ex));
    }

    public Mono<Boolean> verifyOTP(String otp, String userId) {
        return getActiveByUserId(userId)
                .filter(t -> !t.getUpdatedAt().plus(10, ChronoUnit.MINUTES).isBefore(Instant.now()))
                .switchIfEmpty(Mono.error(new BadRequestException("OTPE-002")))
                .filter(t -> t.getToken().equalsIgnoreCase(otp))
                .switchIfEmpty(Mono.error(new BadRequestException("OTPE-003")))
                .then(Mono.just(true));
    }
}
