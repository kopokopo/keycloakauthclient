package client.keycloak.service;

import client.keycloak.model.OneTimePin;
import reactor.core.publisher.Mono;

public interface OneTimePinService {
  Mono<OneTimePin> save(String token, String userId);

  Mono<OneTimePin> getOptional(String userId);

  Mono<OneTimePin> getActiveByUserId(String userId);

  Mono<OneTimePin> updateOTPStatus(String userId, boolean status);

  Mono<Boolean> verifyOTP(String otp, String userId);
}
