package client.keycloak.controller;

import client.keycloak.payload.request.AuthRequest;
import client.keycloak.payload.request.ClientRegistrationRequest;
import client.keycloak.payload.request.DriverRegistrationRequest;
import client.keycloak.payload.request.SendOTPRequest;
import client.keycloak.payload.request.UserCredentialRequest;
import client.keycloak.payload.request.UserRequest;
import client.keycloak.payload.response.CreateUserResponse;
import client.keycloak.payload.response.GenerateOTPResponse;
import client.keycloak.payload.response.TokenResponse;
import client.keycloak.publisher.RabbitMQPublisher;
import client.keycloak.service.OneTimePinServiceImpl;
import client.keycloak.service.UserServiceImpl;
import client.keycloak.util.AccountUtil;
import client.keycloak.util.Groups;
import client.keycloak.util.KeycloakUtil;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import java.util.List;

import static client.keycloak.util.Helpers.generateOTP;
import static client.keycloak.util.Helpers.getAge;

@RestController
@RequestMapping(value = "/api/v1/account/", produces = {"application/json; charset=utf-8"}, consumes = {"application/json"})
public class AccountController {

    private final KeycloakUtil keyCloakUtil;
    private final UserServiceImpl userService;
    private final OneTimePinServiceImpl oneTimePinService;
    private final RabbitMQPublisher rabbitMQPublisher;
    private final AccountUtil accountUtil;

    public AccountController(
            KeycloakUtil keyCloakUtil,
            UserServiceImpl userService,
            OneTimePinServiceImpl oneTimePinService,
            RabbitMQPublisher rabbitMQPublisher,
            AccountUtil accountUtil
    ) {
        this.keyCloakUtil = keyCloakUtil;
        this.userService = userService;
        this.oneTimePinService = oneTimePinService;
        this.rabbitMQPublisher = rabbitMQPublisher;
        this.accountUtil = accountUtil;
    }

    @Operation(summary = "Request oauth2 token from keycloak")
    @PostMapping(value = "token")
    public Mono<TokenResponse> token(@Valid @RequestBody Mono<AuthRequest> request) {
        return request
                .flatMap(r -> keyCloakUtil.getUserToken(r.username(), r.password()));
    }

    @Operation(summary = "Create client account")
    @PostMapping(value = "client")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<CreateUserResponse> registerClient(@Valid @RequestBody ClientRegistrationRequest request) {
        return accountUtil
                .createUser(
                        new UserRequest(
                                request.email(),
                                Boolean.TRUE,
                                Boolean.TRUE,
                                List.of(new UserCredentialRequest("password", request.password(), Boolean.FALSE)),
                                List.of(Groups.CLIENT.name())
                        )
                )
                .zipWith(generateOTP())
                .flatMap(t -> oneTimePinService.save(t.getT2(), t.getT1().getAuthId()))
                .doOnSuccess(t -> rabbitMQPublisher.sendProfileMessage(
                        t.getUserId(),
                        request.name(),
                        null,
                        null,
                        null,
                        request.email()))
                .map(t -> new CreateUserResponse(t.getUserId()));
    }

    @Operation(summary = "Create driver account")
    @PostMapping(value = "driver")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<CreateUserResponse> registerDriver(@Valid @RequestBody DriverRegistrationRequest request) {
        return accountUtil
                .createUser(
                        new UserRequest(
                                request.phoneNumber(),
                                Boolean.TRUE,
                                Boolean.TRUE,
                                List.of(new UserCredentialRequest("password", request.password(), Boolean.FALSE)),
                                List.of(Groups.DRIVER.name())
                        )
                )
                .zipWith(generateOTP())
                .flatMap(t -> oneTimePinService.save(t.getT2(), t.getT1().getAuthId()))
                .doOnSuccess(t -> rabbitMQPublisher.sendProfileMessage(
                        t.getUserId(),
                        request.name(),
                        request.dob(),
                        request.phoneNumber(),
                        getAge(request.dob()),
                        null))
                .doOnSuccess(t -> rabbitMQPublisher.sendDropboxFolderMessage(t.getUserId(), request.phoneNumber()))
                .map(t -> new CreateUserResponse(t.getUserId()));
    }

    @Operation(summary = "Verify OTP token")
    @GetMapping(value = "otp")
    public Mono<GenerateOTPResponse> verifyOTP(
            @RequestParam(name = "uid") String userId,
            @RequestParam(name = "otp") String otp,
            @RequestParam(name = "vtId") String verificationTypeId
    ) {
        return oneTimePinService
                .verifyOTP(otp, userId)
                .flatMap(s -> oneTimePinService.updateOTPStatus(userId, !s))
                .doOnSuccess(s -> rabbitMQPublisher.sendVerificationMessage(userId, !s.isActive(), verificationTypeId))
                .map(o -> new GenerateOTPResponse(!o.isActive()));
    }


    @Operation(summary = "Create OTP token")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "otp")
    public Mono<Void> sendOTP(@Valid @RequestBody SendOTPRequest request) {
        return userService
                .getByAuthId(request.userId())
                .zipWith(generateOTP())
                .flatMap(t -> oneTimePinService.save(t.getT2().toUpperCase(), request.userId()))
                .then();
    }
}
