CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

SELECT uuid_generate_v1mc();

CREATE TABLE IF NOT EXISTS users
(
    id varchar not null primary key DEFAULT uuid_generate_v1mc(),
    authId varchar(100) not null,
    createdAt   timestamp    not null,
    updatedAt   timestamp    not null
);

CREATE TABLE IF NOT EXISTS oneTimePin
(
    id varchar not null primary key DEFAULT uuid_generate_v1mc(),
    token varchar(10) not null,
    userId varchar(40) not null,
    active boolean      not null,
    createdAt   timestamp    not null,
    updatedAt   timestamp    not null
);